import type { ColumnMap } from "@/components/PatientTable.vue";

export function getNameMappings(clinicId: string): Promise<ColumnMap>
export function getClinicNameMappings(): Promise<{ [key: string]: string }>
export function getRecommendation(clinicId: string, page_size: number, page_number): Promose<Patient[]>
export const fakeDatabase: Patient[]
