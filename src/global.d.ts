export interface Patient {
  clinicId: string,
  patientAttributes: {
    [key: string]: string
    account_number: string,
    date_of_birth: string,
    gender: string,
  },
  recDate: string,
  recommendedProcedures: Array<{
    name: string,
  }>,
}

export interface ClinicNameMap {
  [key: string]: string
}